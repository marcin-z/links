# Tools

## IDE

### Visual Studio Code

- [C#](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
- [GitLens � Git supercharged](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)
- [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)
- [Angular 10 Snippets](https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode)
- [TSLint](https://marketplace.visualstudio.com/items?itemName=eg2.tslint)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Azure Functions](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azurefunctions)
- [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)&[Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
- [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)  
- [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)  
- [Remote - WSL](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl)
- [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
- [Bookmarks](https://marketplace.visualstudio.com/items?itemName=alefragnani.Bookmarks)
- [Path Intelisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)


### Visual Studio

- [VS color output](https://marketplace.visualstudio.com/items?itemName=MikeWard-AnnArbor.VSColorOutput)  
- [SQL Complete](https://marketplace.visualstudio.com/items?itemName=DevartSoftware.dbForgeSQLCompleteExpress)  
- [Productivity power tools](https://marketplace.visualstudio.com/items?itemName=VisualStudioProductTeam.ProductivityPowerPack2017)

## API tools
- [POSTMAN](https://www.getpostman.com/apps)
- [Fiddler](https://www.telerik.com/fiddler)
- [NSwag](https://github.com/RSuter/NSwag)
- [Fake Online REST API for Testing and Prototyping](https://jsonplaceholder.typicode.com)

## Azure  
### Tools  
- [Service Bus Explorer](https://github.com/paolosalvatori/ServiceBusExplorer)  
- [Azure Storage Explorer](https://azure.microsoft.com/en-us/features/storage-explorer/)  
- [Azure Cosmos DB Emulator](https://aka.ms/cosmosdb-emulator)

### Knowledge   
- [Azure Tips and Tricks](https://microsoft.github.io/AzureTipsAndTricks/)


## AWS
- [Getting started](https://aws.amazon.com/getting-started/)  
- [Hands-on training - Qwiklabs](https://www.qwiklabs.com/home?locale=en)  

## Web tools
- [JSON to C# generator](https://quicktype.io) 
- [Online diagrams](https://www.draw.io/)  
- [Regex 101](https://regex101.com)  
- [CodePen](https://codepen.io/#)  
- [jSFiddle](https://jsfiddle.net/)  
- [Fake Online REST API for Testing and Prototyping](https://jsonplaceholder.typicode.com)

## Other tools
- [dotPeek](https://www.jetbrains.com/decompiler/?fromMenu)
- [Sysinternals - bundle](https://download.sysinternals.com/files/SysinternalsSuite.zip)
- [CURL Command](https://linuxhandbook.com/curl-command-examples/)  
- [NimbleText](https://nimbletext.com) - text manipulation and code generation tool 
- [KDiff3](http://kdiff3.sourceforge.net)
- [LinqPad](https://www.linqpad.net)
- [Screen To Gif](https://www.screentogif.com) - Screen, webcam and sketchboard recorder with an integrated editor
- [PNGGauntlet](https://pnggauntlet.com) - PNG compressor

# Learning  

## Blogs  
- [Andrew Lock | .NET Escapades](https://andrewlock.net)  
- [Steve Gordon](https://www.stevejgordon.co.uk)    
- [ASP.NET Hacker](https://asp.net-hacker.rocks)
- [Exception Not Found](https://exceptionnotfound.net/)  
- [Build please](https://buildplease.com/)  
- [@herbertograca](https://herbertograca.com/)  
- [The Reformed Programmer](http://www.thereformedprogrammer.net)  
- [Coding in helmet](http://www.codinghelmet.com)  
- [Particular blog](https://particular.net/videos)  
- [Sapiensworks blog](http://blog.sapiensworks.com)  
- [Practical DDD](http://practical-ddd.blogspot.hu)  
- [ASP.NET Core Series](https://tahirnaushad.com/2017/08/16/-asp-net-core-problem-solution-series/)  
- [Enterprise Craftsmanship blog](https://enterprisecraftsmanship.com)  
- [Yet Another Developer Blog](https://www.tpeczek.com)  
- [Why you should learn ASP.NET Core](https://codingblast.com/why-asp-net-core/)  
- [Tomáš Herceg](https://tomasherceg.com/blog)  
- [ASP.NET-hacker](https://asp.net-hacker.rocks/archive.html)  
- [Productive Rage](https://www.productiverage.com)
- [Code Rethinked](https://coderethinked.com)
- [Code Journey - .NET Internals series](https://www.codejourney.net/tag/dotnet-internals/)
- [Wojcieszyn blog](https://www.strathweb.com)
- [Kamil Grzybek](http://www.kamilgrzybek.com)
- [Michael Crump - Azure Dev Tips](https://www.michaelcrump.net)
- [Damien Bod on web development](https://damienbod.com)

## Practice  
- [LINQ samples](http://linqsamples.com)  
- [Codewars](https://www.codewars.com)  
- [codecademy](https://www.codecademy.com) 
- [Exercism](https://exercism.io)   

## Boilerplates  
- [boilrplate](http://www.boilrplate.com)  

## Git, CI, CD  
- [Git recipes](http://ohshitgit.com)  

## Security  
- [NetSPI SQL Injection Wiki](https://sqlwiki.netspi.com)  

## Podcast  
- [Dotnet new podcast](https://dotnetcore.show)  
- [ThoughtWorks Podcast](https://www.thoughtworks.com/podcasts)  
- [.NET Rocks! vNext](https://dotnetrocks.com/)

## Docker
- [Docker CheatSheet](https://dockercheatsheet.painlessdocker.com/)  

# ebooks  

- [Little ASP.NET Core book](https://www.recaffeinate.co/book/)  
- [Exploring .NET Core with Microservices,  ASP.NET Core, and Entity Framework Core ](https://www.manning.com/books/exploring-dot-net-core)  

# Other
## Public/Free APIs
- [Public API](https://public-apis.xyz/category/media)   
- [API-list](https://apilist.fun)

## English
-   [English cheatsheets](https://www.englishclass101.com/learn-with-pdf)  

# Databses
- [Glenn's SQL Server Performance](https://glennsqlperformance.com/resources/)

# Repositories
## Code practice
- [AspNet Core Diagnostic Scenarios](https://github.com/davidfowl/AspNetCoreDiagnosticScenarios/blob/master/AspNetCoreGuidance.md)
- [AspNet Core async guidance](https://github.com/davidfowl/AspNetCoreDiagnosticScenarios/blob/master/AsyncGuidance.md)
- [Awessome .NET Core](https://github.com/thangchung/awesome-dotnet-core/)
  
## Open source - Sample Apps
  - [nopCommerce](https://github.com/nopSolutions/nopCommerce)
  - [OrchardCore](https://github.com/OrchardCMS/OrchardCore)
  - [SimplCommerce](https://github.com/simplcommerce/SimplCommerce)
  - [squidex - CMS](https://github.com/Squidex/squidex)
  - [Miniblog.Core](https://github.com/madskristensen/Miniblog.Core)
  - [Blogifier](https://github.com/blogifierdotnet/Blogifier)
  - [eShopOnContainers](https://github.com/dotnet-architecture/eShopOnContainers)
  - [eShopOnWeb](https://github.com/dotnet-architecture/eShopOnWeb)
  - [Northwind Traders](https://github.com/JasonGT/NorthwindTraders)
  - [ReactiveTraderCloud](https://github.com/AdaptiveConsulting/ReactiveTraderCloud)  
  - [Practical ASP.NET Core](https://github.com/dodyg/practical-aspnetcore) 
  - [Northwind EF Core 3](https://github.com/brendan-ssw/)
  - [Sample App](https://github.com/VaughnVernon/IDDD_Samples_NET)
  - [Equinox Project](https://github.com/EduardoPires/EquinoxProject)
  - [Enterprise Planner](https://github.com/gfawcett22/EnterprisePlanner)
  - [GraphQL Example - StarWars](https://github.com/JacekKosciesza/StarWars)
  - [sample-dotnet-core-cqrs-api](https://github.com/kgrzybek/sample-dotnet-core-cqrs-api)
  - [Microservices- sample - POS](https://github.com/NHadi/Pos)


## DDD CQRS Microservices sample code
- [Monolith modular DDD](https://github.com/kgrzybek/modular-monolith-with-ddd)
- [Altcom microservices](https://github.com/asc-lab/dotnetcore-microservices-poc) 
- [EventFlow](https://github.com/eventflow/EventFlow)
- [PitStop](https://github.com/EdwinVW/pitstop)
